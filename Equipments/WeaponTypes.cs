﻿namespace Assignment.Equipments
{
    public  enum WeaponTypes
    {
        AXE,
        BOW,
        HAMMER,
        SWORD,
        DAGGER,
        STAFF, 
        WAND

    }
}

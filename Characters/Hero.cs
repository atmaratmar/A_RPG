﻿using System.Collections.Generic;
using System.Linq;
using Assignment.Characters.Services;
using Assignment.Equipments;
using Assignment.Exceptions;

namespace Assignment.Characters
{
    /// <summary>
    /// CharacterBaseClass the parent class for all characters
    /// </summary>
    public abstract class Hero : Services.Services
    {
        protected Hero() { }
        public string Name;
        public int Level;
        public double Dps { get; set; }
        protected Hero(string name)
        {
            Name = name;
            Level = 1;
            PrimAttributes = new PrimaryAttributes();
            SecondaryAttributes = new SecondaryAttributes();
        }
        public Dictionary<Slots,Item> EquipmentTools =new()
        {{Slots.HEAD, new Armor()}, {Slots.BODY, new Armor()}, 
            {Slots.LEGS, new Armor()}, {Slots.WEAPON, new Weapon()}};
        /// <summary>
        /// Set Armor base on the level
        /// </summary>
        /// <param name="armor"></param>
        /// <returns></returns>
            public string SetArmor(Armor armor){
            if (armor.ItemLevel > Level)
            {
                throw new EquipmentException
                ("At level " + Level.ToString() + " you can't get this Armor");
            }
            if (!ArmorTypes.Contains(armor.ArmorType))
            {
                throw new EquipmentException("Armor is not available ");
            }
            EquipmentTools[armor.ItemSlot] = armor;
            return "Armor added"; }
        /// <summary>
        /// Set Weapon base on the level
        /// </summary>
        /// <param name="Weapon"></param>
        /// <returns></returns>
        public string SetWeapon(Weapon Weapon) {
            if (Weapon.ItemLevel > Level)
            {
                return "At level "+Level.ToString()+ " you can't get this weapon";
            }
            if (WeaponTypes.Contains(Weapon.WeaponType))
            {
                throw new EquipmentException("weapon is not available");
            }
            EquipmentTools[Weapon.ItemSlot] = Weapon;
            return "Weapon added"; }
        public abstract void CalculateDps();

    }
}

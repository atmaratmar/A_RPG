﻿namespace Assignment.Characters
{
    /// <summary>
    /// Primary attributes are what determine the characters power, as the character levels up their primary attributes 
    /// increase.Items they equip also can increase these primary attributes(this is detailed in Appendix B). Secondary
    ///attributes affect the characters’ ability to survive, they are calculated from primary attributes.Certain character classes
    ///are more durable against certain types of damage.
    /// </summary>
    public class PrimaryAttributes
    {
        /// <summary>
        /// created constractor to invoked automatically at the time of object creation.
        /// </summary>
        /// <param name="vitality"></param>
        /// <param name="strength"></param>
        /// <param name="dexterity"></param>
        /// <param name="intelligence"></param>
        public PrimaryAttributes(int vitality,
            int strength,
            int dexterity,
            int intelligence)
        {
            Vitality = vitality;
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }
        public PrimaryAttributes()
        {
        }
        /// <summary>
        /// primary attributes 
        /// </summary>
        public int Vitality { get; set; }

        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
    }

}

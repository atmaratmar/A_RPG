﻿using System.Collections.Generic;
using System.Diagnostics;
using Assignment.Characters.Services;
using Assignment.Equipments;

namespace Assignment.Characters
{
    public class Rogue : Hero
    {
        public Rogue(string name) : base(name)
        {
            SetStartsStats();
        }
        /// <summary>
        /// level up 
        /// </summary>
        public override void LevelUp()
        {
            Level++;
            PrimAttributes.Vitality += 3;
            PrimAttributes.Strength += 1;
            PrimAttributes.Dexterity += 4;
            PrimAttributes.Intelligence += 1;
        }
        /// <summary>
        /// Initial state
        /// </summary>
        public override void SetStartsStats()
        {
            PrimAttributes.Vitality = 8;
            PrimAttributes.Strength = 2;
            PrimAttributes.Dexterity = 6;
            PrimAttributes.Intelligence = 1;
            ArmorTypes = new List<ArmorTypes>
            {
                Equipments.ArmorTypes.LEATHER,
                Equipments.ArmorTypes.MAIL
            };
            WeaponTypes = new List<WeaponTypes>
            {
                Equipments.WeaponTypes.SWORD,
                Equipments.WeaponTypes.DAGGER,
            };

        }
        /// <summary>
        /// Secondary Attributes
        /// </summary>
        public override void SecondaryAttributesState()
        {
            SecondaryAttributes.Health = PrimAttributes.Vitality * 10;
            SecondaryAttributes.ArmorRating = PrimAttributes.Strength + PrimAttributes.Dexterity;
            SecondaryAttributes.ElementalResistance = PrimAttributes.Intelligence;
        }
        /// <summary>
        /// Calculate DPS
        /// </summary>
        public override void CalculateDps()
        {
            Weapon equippedWeapon = EquipmentTools[Slots.WEAPON] as Weapon;
            Debug.Assert(equippedWeapon != null, nameof(equippedWeapon) + " != null");
            Dps = (equippedWeapon.WeaponAttributes.AttackSpeed * equippedWeapon.WeaponAttributes.Damage) *
                  (1 + PrimAttributes.Strength / 100.0);
        }
    }
}

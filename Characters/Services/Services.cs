﻿using System.Collections.Generic;
using Assignment.Equipments;

namespace Assignment.Characters.Services
{
    public abstract class Services
    {
        public abstract void LevelUp();
        public abstract void SetStartsStats();
        public abstract void SecondaryAttributesState();
        public PrimaryAttributes PrimAttributes;
        public SecondaryAttributes SecondaryAttributes;
        public List<WeaponTypes> WeaponTypes;
        public List<ArmorTypes> ArmorTypes;
    }
}

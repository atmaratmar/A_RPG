﻿using Assignment.Equipments;
using System.Collections.Generic;
using System.Diagnostics;
using Assignment.Characters.Services;

namespace Assignment.Characters
{
    public class Mage : Hero
    { 
        public Mage(string name): base(name)
        {
            SetStartsStats();
        }
        /// <summary>
        /// level up 
        /// </summary>
        public override void LevelUp()
        {
            Level++;
            PrimAttributes.Vitality += 3;
            PrimAttributes.Strength += 1;
            PrimAttributes.Dexterity += 1;
            PrimAttributes.Intelligence += 5;
        }
        /// <summary>
        /// Initial state
        /// </summary>
        public sealed override void SetStartsStats()
        {
            ArmorTypes = new List<ArmorTypes>
            {
                Equipments.ArmorTypes.CLOTH
            };
            WeaponTypes = new List<WeaponTypes> {Equipments.WeaponTypes.STAFF, Equipments.WeaponTypes.WAND};
            PrimAttributes.Vitality = 5;
            PrimAttributes.Strength = 1;
            PrimAttributes.Dexterity = 1;
            PrimAttributes.Intelligence = 8;
        }
        /// <summary>
        /// Secondary Attributes
        /// </summary>
        public override void SecondaryAttributesState()
        {
            SecondaryAttributes.Health = PrimAttributes.Vitality * 10;
            SecondaryAttributes.ArmorRating = PrimAttributes.Strength + PrimAttributes.Dexterity;
            SecondaryAttributes.ElementalResistance = PrimAttributes.Intelligence;
        }
        /// <summary>
        /// Calculate DPS
        /// </summary>
        public override void CalculateDps()
        {
            Weapon equippedWeapon = EquipmentTools[Slots.WEAPON] as Weapon;
            Debug.Assert(equippedWeapon != null, nameof(equippedWeapon) + " != null");
            Dps = (equippedWeapon.WeaponAttributes.AttackSpeed * equippedWeapon.WeaponAttributes.Damage) *
                  (1 + PrimAttributes.Strength / 100.0);
        }
    }
}
